try:
    from datadog_checks.base import AgentCheck
except ImportError:
    from checks import AgentCheck

import os

__version__ = "1.0.0"

class TempCheck(AgentCheck):
    def check(self, instance):
        temp = os.popen('cat /sys/class/thermal/thermal_zone0/temp')
        self.gauge('node.temp', float(temp.read())/1000, tags=['TAG_KEY:TAG_VALUE'])
