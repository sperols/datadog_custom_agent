FROM gcr.io/datadoghq/agent

COPY conf.d/* /conf.d/
COPY checks.d/* /checks.d/
